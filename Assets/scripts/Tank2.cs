﻿using UnityEngine;
using System.Collections;

public class Tank2 : MonoBehaviour {
    private const int leftMouseId = 2;
    private const int rightMouseId = 3;
	private Vector2 left_rotation;
    private Vector2 right_rotation;
	public float speed = 1;
	public float rot_speed = 1;
	// Use this for initialization

	void Start () {
	    left_rotation[0] = 0;
	    left_rotation[1] = 0;
	    right_rotation[0] = 0;
	    right_rotation[1] = 0;
		Cursor.lockState = CursorLockMode.Confined;

	}
	
	// Update is called once per frame
	void FixedUpdate () {
		Vector2 new_left_rotation = MouseInput.move[leftMouseId];
	    Vector2 left_diff = new_left_rotation - left_rotation;
	    Vector2 new_right_rotation = MouseInput.move[rightMouseId];
	    Vector2 right_diff = new_right_rotation - right_rotation;
	    
	    transform.Rotate(0, 0, Time.deltaTime * rot_speed * (left_diff[1] - right_diff[1]));

	    float movementSpeed = speed * (left_diff[1] + right_diff[1]);
	    transform.Translate(Mathf.Sin(transform.eulerAngles.y) * movementSpeed * Time.deltaTime, Mathf.Cos(transform.eulerAngles.y) * movementSpeed * Time.deltaTime, 0);

	    left_rotation[0] = 0;
	    left_rotation[1] = 0;
	    right_rotation[0] = 0;
	    right_rotation[1] = 0;
	    MouseInput.move[leftMouseId][0] = 0;
	    MouseInput.move[rightMouseId][0] = 0;

	    MouseInput.move[leftMouseId][1] = 0;
	    MouseInput.move[rightMouseId][1] = 0;
	}
}